===========================================================
=====						      =====
=====              Assignment 1: Shading              =====
=====						      =====
=====		    Wesley To CS184-CS		      =====
=====		   Jonathan Ma CS184-CY		      =====
=====						      =====
===========================================================

This project was developed and run in Visual Studio Professional 2012.

The code was submitted from Wesley To's (cs184-cs) account.

Our general approach to the was to divide and conquer. We initially created helper functions to do the mathematical work, such as dot and cross products, or component wise arithmetic. Next, we set up the command line parser to store the arguments in global variables that would trigger/be accessible by the other functions. We then set up any variables that needed to be passed in to each calculation, such as surface position and or viewer position. Next, we implemented ambient(), diffuse(), and specular() using the helper functions and the aforementioned variables. Finally, we added ambient(), diffuse(), and specular() calculations into circle(), summed them together, and triggered them with boolean values (ambientOn, diffuseOn, specularOn) that were set the the command line parser.



===========================================================
=====                  Toon Shader                    =====
===========================================================

Toon shading - "-ts [gradient]"

A gradient level in [5, 20] is recommended for the best results. Also works best with some level of ambient.

The toon shader is triggered with an argument in the form of "-ts [int]" where int is the level of gradiation in the image. In essence, the level of gradiation is equal to the number of colors per RGB channel one expects to see in the toon shaded version (ie. "-ts 7" means there will be 7 red shades, 7 green shades, and 7 blue shades). The toon shader acts on top of the other shaders and will gradiate the level of shading post-calculation. Specular toon shading will have its gradient level cut in half, which we felt produced better results. Gradient levels of 0 will produce a gray outline of the shape. Negative gradient levels will also be accepted, but will be converted to the positive equivalent. Excessively high levels of gradiation will make the resulting image approach the result without toon shading whatsoever.

Results: 

shaded green sphere with ambient, diffuse, and specular components.
-ts 15 -ka 0.05 0.05 0.05 -kd 0.05 1 0.5 -ks 0.1 0.1 0.1 -pl -1.0 0.0 1.0 0.5 0.5 0.5 -dl 1.0 0.0 1.0 0.5 0.5 0.5 -dl -1.0 0.0 -1.0 0.5 0.5 0.5 -sp 1.0

Shaded purple sphere with diffuse and specular components and a single light source.
-ts 10 -ka 0 0 0 -kd 1 0 1 -ks 0 0 1 -sp 20 -pl 1 1 1 1 1 1

Toon shaded pokeball
-ts 10 -ka .03 .02 .02 -kd 1.0 1.0 1.0 -ks .65 .65 .65 -pl 0.5 -6.0 0.5 .7 .7 .7 -pl -0.5 6.0 -0.5 .7 0.0 0.0 -pl 0.0 0.0 1.04 .5 .5 .5 -sp 20.0

Toon shaded rainbow sphere
-ts 5 -ka .03 .02 .02 -kd 1.0 1.0 1.0 -ks .65 .65 .65 -sp 20.0 -pl 0 4 10 2 0 0 -pl -4 -4 10 0 2 0 -pl 4 -4 10 0 0 2

===========================================================
===========================================================
