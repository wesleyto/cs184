
#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>
#include <stdlib.h>
#include <stdio.h>
#include <algorithm>
#include <string.h>

#ifdef _WIN32
#include <windows.h>
#else
#include <sys/time.h>
#endif

#ifdef OSX
#include <GLUT/glut.h>
#include <OpenGL/glu.h>
#else
#include <GL/glut.h>
#include <GL/glu.h>
#endif

#include <time.h>
#include <math.h>


#define PI 3.14159265  // Should be used from mathlib
inline float sqr(float x) { return x*x; }

using namespace std;

//****************************************************
// Some Classes
//****************************************************

class Viewport;

class Viewport {
  public:
    int w, h; // width and height
};


//****************************************************
// Global Variables
//****************************************************
Viewport	viewport;
float* pntLights = new float[30];
float* dirLights = new float[30];
int dlCount = 0;
int plCount = 0;
bool ambientOn = false;
bool diffuseOn = false;
bool specularOn = false;
bool toonOn = false;
float ka[] = {0.0, 0.0, 0.0};
float kd[] = {0.0, 0.0, 0.0};
float ks[] = {0.0, 0.0 ,0.0};
float sp = 0.0;
int ts = 0;



//****************************************************
// Declare our functions
//****************************************************
float* ambient(float*);
float* diffuse(float*, float*, float*);
float* specular(float*, float*, float*, float*);
float* toon(float*, int);
float vDot(float*, float*);
float* vSub(float*, float*);
float* vAdd(float*, float*);
float* normalize(float*);
int* floatToInt(float*);
float magnitude(float*);
void vPrint(float*);
float* scale(float, float*);
float* vSub(float*, float*);
void spaceExit(unsigned char, int, int);

//****************************************************
// Simple init function
//****************************************************
void initScene(){

  // Nothing to do here for this simple example.

}


//****************************************************
// reshape viewport if the window is resized
//****************************************************
void myReshape(int w, int h) {
  viewport.w = w;
  viewport.h = h;

  glViewport (0,0,viewport.w,viewport.h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0, viewport.w, 0, viewport.h);

}


//****************************************************
// A routine to set a pixel by drawing a GL point.  This is not a
// general purpose routine as it assumes a lot of stuff specific to
// this example.
//****************************************************

void setPixel(int x, int y, GLfloat r, GLfloat g, GLfloat b) {
  glColor3f(r, g, b);
  glVertex2f(x + 0.5, y + 0.5);   // The 0.5 is to target pixel
  // centers 
  // Note: Need to check for gap
  // bug on inst machines.
}

//****************************************************
// Draw a filled circle.  
//****************************************************


void circle(float centerX, float centerY, float radius) {
  // Draw inner circle
  glBegin(GL_POINTS);

  // We could eliminate wasted work by only looping over the pixels
  // inside the sphere's radius.  But the example is more clear this
  // way.  In general drawing an object by loopig over the whole
  // screen is wasteful.

  int i,j;  // Pixel indices
  int minI = max(0,(int)floor(centerX-radius));
  int maxI = min(viewport.w-1,(int)ceil(centerX+radius));
  int minJ = max(0,(int)floor(centerY-radius));
  int maxJ = min(viewport.h-1,(int)ceil(centerY+radius));

  for (i=minI;i<maxI;i++) {
    for (j=minJ;j<maxJ;j++) {

      // Location of the center of pixel relative to center of sphere
      float x = (i+0.5-centerX);
      float y = (j+0.5-centerY);

      float dist = sqrt(sqr(x) + sqr(y));

      if (dist<=radius) {

        // This is the front-facing Z coordinate
        float z = sqrt(radius*radius-dist*dist);
		
		float surfaceDir[] = {x, y, z};
		float center[] = {centerX, centerY, 0};

		// default color
		float* color = new float[3];
		color[0] = 0.0;
		color[1] = 0.0;
		color[2] = 0.0;

		//orthographic viewer
		static float* viewer = new float[3];
		viewer[0] = 0.0;
		viewer[1] = 0.0;
		viewer[2] = -1.0;
		

		//loops over directional lights and calculates light-dependent values
		for (int dl = 0; dl < dlCount; dl++) {

			// diffuse position and colors
			float* lightDir = new float[3];
			lightDir[0] = dirLights[6*dl];
			lightDir[1] = dirLights[6*dl+1];
			lightDir[2] = dirLights[6*dl+2];
			float* lightColor = new float[3];
			lightColor[0] = dirLights[6*dl+3];
			lightColor[1] = dirLights[6*dl+4];
			lightColor[2] = dirLights[6*dl+5];

			if (ambientOn) {
				float* amb = ambient(lightColor);
				color = vAdd(color, amb);
			}
			if (diffuseOn) {
				float* diff = diffuse(surfaceDir, lightDir, lightColor);
				if (toonOn) {
					color = vAdd(toon(diff, ts), color);
				}
				else {
					color = vAdd(color, diff);
				}
			}
			if (specularOn) {
				float* spec = specular(surfaceDir, viewer, lightDir, lightColor);
				if (toonOn) {
					color = vAdd(toon(spec, ts/2), color);
				}
				else {
					color = vAdd(color, spec);
				}
			}

			free(lightColor);
			free(lightDir);

		}


		//loops over point lights and calculates light-dependent values
		for (int pl = 0; pl < plCount; pl++) {

			// sets diffuse light position and colors from the stored values
			float* lightPos = new float[3];
			lightPos[0] = radius * pntLights[6*pl];
			lightPos[1] = radius * pntLights[6*pl+1];
			lightPos[2] = radius * pntLights[6*pl+2];
			float* lightColor = new float[3];
			lightColor[0] = pntLights[6*pl+3];
			lightColor[1] = pntLights[6*pl+4];
			lightColor[2] = pntLights[6*pl+5];
			float * lightDir = vSub(surfaceDir, lightPos);


			//ambient is calculated once for each pixel
			if (ambientOn) {
				float* a = ambient(lightColor);
				color = vAdd(color, a);
			}

			if (diffuseOn) {
				float* diff = diffuse(surfaceDir, lightDir, lightColor);
				
				if (toonOn) {
					color = vAdd(toon(diff, ts), color);
				}
				else {
					color = vAdd(color, diff);
				}
			}

			if (specularOn) {
				float* spec = specular(surfaceDir, viewer, lightDir, lightColor);
				if (toonOn) {
					color = vAdd(toon(spec, ts/2), color);
				}
				else {
					color = vAdd(color, spec);
				}
			}
			free(lightPos);
			free(lightColor);
			free(lightDir);

		}

        setPixel(i,j, color[0], color[1], color[2]);
        // This is amusing, but it assumes negative color values are treated reasonably.
        // setPixel(i,j, x/radius, y/radius, z/radius );

		free(color);
      }
    }
  }


  glEnd();
}


//****************************************************
// Internal function ==> print 3D vector
//****************************************************

void vPrint(float* f) {
	cout << "["<<f[0] << ", " <<f[1]<<", " <<f[2]<<"]\n";
}

//****************************************************
// Internal function ==> 3D vector dot product
//****************************************************

float vDot(float* f1, float*f2) {
	return f1[0]*f2[0] + f1[1]*f2[1] + f1[2]*f2[2];
}

//****************************************************
// Internal function ==> angle between 2 3D vectors
//****************************************************

float angle(float* f1, float*f2) {
	return acos(vDot(f1, f2)/(magnitude(f1)*magnitude(f2)));
}

//****************************************************
// Internal function ==> magnitude of a 3D vector
//****************************************************

float magnitude(float* f) {
	return sqrt(f[0]*f[0] + f[1]*f[1] + f[2]*f[2]);
}

//****************************************************
// Internal function ==> 3D vector subtraction
//****************************************************

float* vSub(float* f1, float*f2) {
	float* n = new float[3];
	n[0] = f1[0]-f2[0];
	n[1] = f1[1]-f2[1];
	n[2] = f1[2]-f2[2];
	return n;
}


//****************************************************
// Internal function ==> 3D vector addition
//****************************************************

float* vAdd(float* f1, float*f2) {
	float* n = new float[3];
	n[0] = f1[0]+f2[0];
	n[1] = f1[1]+f2[1];
	n[2] = f1[2]+f2[2];
	return n;
}

//****************************************************
// Internal function ==> 3D vector component-wise multiplication
//****************************************************

float* vMult(float* f1, float*f2) {
	float* n = new float[3];
	n[0] = f1[0]*f2[0];
	n[1] = f1[1]*f2[1];
	n[2] = f1[2]*f2[2];
	return n;
}

//****************************************************
// Internal function ==> normalizes 3D vector
//****************************************************

float* normalize(float* f) {
	float l = magnitude(f);
	float* n = new float[3];
	n[0] = f[0]/l;
	n[1] = f[1]/l;
	n[2] = f[2]/l;
	return n;
}

//****************************************************
// Internal function ==> calculate the cross product of two 3D vectors
//****************************************************

float* vCross(float* f1, float* f2) {
	float* n =  new float[3]; 
	n[0] = (f1[1]*f2[2] - f1[2]*f2[1]);
	n[1] = (f1[2]*f2[0] - f1[0]*f2[2]);
	n[2] = (f1[0]*f2[1] - f1[1]*f2[0]);
	return n;
}

//****************************************************
// Internal function ==> [0-1] floats to [0-255] ints
//****************************************************

int* floatToInt(float* f) {
	int* n = new int[3];
	n[0] = (int)(255*f[0]);
	n[1] = (int)(255*f[1]);
	n[2] = (int)(255*f[2]);
	return n;
}

//****************************************************
// Internal function ==> scales 3D vector
//****************************************************

float* scale(float scalar, float *f) {
	float* n = new float[3];
	n[0] = scalar*f[0];
	n[1] = scalar*f[1];
	n[2] = scalar*f[2];
	return n;
}

//****************************************************
// Calculates ambient values
//****************************************************

float* ambient(float lightColor[]) {
	return vMult(ka, lightColor);

}

//****************************************************
// Calculates diffuse values
//****************************************************

float* diffuse(float surfaceDir[], float lightDir[], float* lightColor) {

	float* n = normalize(surfaceDir);
	float* l = normalize(scale((-1), lightDir));
	float lambert = vDot(n, l);
	// no max function in Ubuntu environment
	if (lambert < 0) {lambert = 0.0;}

	float *diff = vMult(kd, lightColor);

	float* m = new float[3];
	m[0] = lambert*diff[0];
	m[1] = lambert*diff[1];
	m[2] = lambert*diff[2];

	free(n);
	free(l);
	free(diff);

	return m;

}

//****************************************************
// Calculates specular values
//****************************************************

float* specular(float surfaceDir[], float* viewer, float* lightDir, float* lightColor) {

    float* n = normalize(surfaceDir);
    float* l = normalize(lightDir);
    float* r = vSub(scale(2*(vDot(n, l)), n), l);
    float rv = vDot(normalize(r), normalize(viewer));

	// no max function in Ubuntu environment
	if (rv < 0) {rv = 0.0;}
	rv = pow(rv, sp);

	float *spec = vMult(ks, lightColor);

	float* m = new float[3];
	m[0] = rv*spec[0];
	m[1] = rv*spec[1];
	m[2] = rv*spec[2];

	free(n);
	free(l);
	free(r);
	free(spec);

	return m;

}

//****************************************************
// Calculates toon shaded values
//****************************************************

float* toon(float* input, int gradient) {

	if (gradient < 0) {gradient *= (-1);}
	float min_brightness = 1.0/255.0;

	int a = (int)((gradient-1) * input[0]);
	int b = (int)((gradient-1) * input[1]);
	int c = (int)((gradient-1) * input[2]);

	float* m = new float[3];
	m[0] = min_brightness+((float)a/(gradient-1));
	m[1] = min_brightness+((float)b/(gradient-1));
	m[2] = min_brightness+((float)c/(gradient-1));

	return m;

}




//****************************************************
// function that does the actual drawing of stuff
//***************************************************
void myDisplay() {

  glClear(GL_COLOR_BUFFER_BIT);				// clear the color buffer

  glMatrixMode(GL_MODELVIEW);			        // indicate we are specifying camera transformations
  glLoadIdentity();				        // make sure transformation is "zero'd"

  // Start drawing
  circle(viewport.w / 2.0 , viewport.h / 2.0 , min(viewport.w, viewport.h) / 3.0);

  glFlush();
  glutSwapBuffers();					// swap buffers (we earlier set double buffer)
}


//****************************************************
// function that exits upon 'space' keypress
//***************************************************
void spaceExit(unsigned char c, int x, int y) {
  if (c == ' ') {
	  exit(0);
  }
}



//****************************************************
// the usual stuff, nothing exciting here
//****************************************************
int main(int argc, char *argv[]) {
  //This initializes glut
  glutInit(&argc, argv);


    for (int i = 1; i < argc; i++) {
    if(strcmp(argv[i], "-ka") == 0){
		ambientOn = true;
	     ka[0] = atof(argv[i+1]);
	     ka[1] = atof(argv[i+2]);
	     ka[2] = atof(argv[i+3]);
	     i = i + 3;
    }
    else if(strcmp(argv[i], "-kd") == 0){
		diffuseOn = true;
		kd[0] = atof(argv[i+1]);
		kd[1] = atof(argv[i+2]);
		kd[2] = atof(argv[i+3]);
		i = i + 3;
    } 
	else if(strcmp(argv[i], "-ks") == 0){
		specularOn = true;
		ks[0] = atof(argv[i+1]);
		ks[1] = atof(argv[i+2]);
		ks[2] = atof(argv[i+3]);
		i = i + 3;
    }
	else if(strcmp(argv[i], "-sp") == 0){
		sp = atof(argv[i+1]);
		i = i + 1;
    } 
	else if(strcmp(argv[i], "-dl") == 0) {
		
		dirLights[dlCount*6] = atof(argv[i+1]);
		dirLights[dlCount*6+1] = atof(argv[i+2]);
		dirLights[dlCount*6+2] = atof(argv[i+3]);
		dirLights[dlCount*6+3] = atof(argv[i+4]);
		dirLights[dlCount*6+4] = atof(argv[i+5]);
		dirLights[dlCount*6+5] = atof(argv[i+6]);
		dlCount++;
		i = i + 6;
	}
	else if(strcmp(argv[i], "-pl") == 0) {
		
		pntLights[plCount*6] = atof(argv[i+1]);
		pntLights[plCount*6+1] = atof(argv[i+2]);
		pntLights[plCount*6+2] = atof(argv[i+3]);
		pntLights[plCount*6+3] = atof(argv[i+4]);
		pntLights[plCount*6+4] = atof(argv[i+5]);
		pntLights[plCount*6+5] = atof(argv[i+6]);
		plCount++;
		i = i + 6;
	}
	else if(strcmp(argv[i], "-ts") == 0) {
		toonOn = true;
		ts = atoi(argv[i+1]);
		i++;
	}
  }
  

  //This tells glut to use a double-buffered window with red, green, and blue channels 
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
  
  // Initalize theviewport size
  viewport.w = 400;
  viewport.h = 400;

  //The size and position of the window
  glutInitWindowSize(viewport.w, viewport.h);
  glutInitWindowPosition(-viewport.w, -viewport.h);
  glutCreateWindow(argv[0]);

  initScene();							// quick function to set up scene

  glutDisplayFunc(myDisplay);				// function to run when its time to draw something
  glutReshapeFunc(myReshape);				// function to run when the window gets resized
  glutKeyboardFunc(spaceExit);				// function to run to check for spacebar keypress

  glutMainLoop();							// infinite loop that will keep drawing and resizing
  // and whatever else


  return 0;
}
