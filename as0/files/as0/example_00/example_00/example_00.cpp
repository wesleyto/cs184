// CS184 Simple OpenGL Example
#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>

#ifdef _WIN32
#include <windows.h>
#else
#include <sys/time.h>
#endif

#ifdef OSX
#include <GLUT/glut.h>
#include <OpenGL/glu.h>
#else
#include <GL/glut.h>
#include <GL/glu.h>
#endif

#include <time.h>
#include <math.h>

#ifdef _WIN32
static DWORD lastTime;
#else
static struct timeval lastTime;
#endif

#define PI 3.14159265

using namespace std;

//****************************************************
// Some Classes
//****************************************************
class Viewport {
public:
	int w, h; // width and height
};


//****************************************************
// Global Variables
//****************************************************
Viewport    viewport;

//****************************************************
// reshape viewport if the window is resized
//****************************************************
void myReshape(int w, int h) {
	viewport.w = w;
	viewport.h = h;

	glViewport(0,0,viewport.w,viewport.h);// sets the rectangle that will be the window
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();                // loading the identity matrix for the screen

	//----------- setting the projection -------------------------
	// glOrtho sets left, right, bottom, top, zNear, zFar of the chord system


	// glOrtho(-1, 1 + (w-400)/200.0 , -1 -(h-400)/200.0, 1, 1, -1); // resize type = add
	// glOrtho(-w/400.0, w/400.0, -h/400.0, h/400.0, 1, -1); // resize type = center

	glOrtho(-1, 1, -1, 1, 1, -1);    // resize type = stretch

	//------------------------------------------------------------
}


//****************************************************
// sets the window up
//****************************************************
void initScene(){
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // Clear to black, fully transparent

	myReshape(viewport.w,viewport.h);
}


//***************************************************
// function that does the actual drawing
//***************************************************
void myDisplay() {


	//----------------------- ----------------------- -----------------------
	// This is a quick hack to add a little bit of animation.
	/*static float tip = 0.5f;
	const  float stp = 0.0001f;
	const  float beg = 0.1f;
	const  float end = 0.9f;

	tip += stp;
	if (tip>end) tip = beg;*/
	//----------------------- ----------------------- -----------------------


	glClear(GL_COLOR_BUFFER_BIT);                // clear the color buffer (sets everything to black)

	glMatrixMode(GL_MODELVIEW);                  // indicate we are specifying camera transformations
	glLoadIdentity();                            // make sure transformation is "zero'd"


	//----------------------- code to draw objects --------------------------

	static int cycle = 0; //controls which animation to play. Toggled with spacebar.

	// number of stars (points) to draw at a time
	const int num_stars = 60;

	//stores positions, speeds, and angles of 14 stars (as points along linear eqtn's)
	static float pos[num_stars]; // positions along x axis
	static float spd[num_stars]; //amounts to move along x-axis
	static float angle[num_stars]; //slopes of linear eqtn
	static float stars[num_stars-(num_stars%2)]; //static star positions, in pairs.

	// allows if-clause only on first loop
	static char flag = 1;
	if (flag) { //initialize random values in arrays
		for (int i = 0; i<num_stars; i++) {
			pos[i] = 0.0f; // zero out array
			spd[i] = (((float)((rand()%9)-4.0f))/1000.0f); // random float in [-.005, .005], excludes zero.
			angle[i] = pow((((float)((rand()%353)-171.0f))/100.0f), 3); //random float in ~[-5, 5]
			if (spd[i]==0.0f) {spd[i] = 0.0001;}
		}
		for (int i = 0; i<num_stars-(num_stars%2); i++) {
			stars[i] = ((float)((rand()%20001)-10000))/10000.0f;
		}
	}
	// no longer allows above if-clause
	flag = 0;

	//loop to draw static stars
	for (int i = 0; i<num_stars-(num_stars%2); i+=2) {
		glColor3f(1.0f, 1.0f, 1.0f);
		glBegin(GL_POINTS);
		glVertex2f(stars[i], stars[i+1]);
		glEnd();
	}

	//loop to draw and update moving star positions
	for (int i = 0; i<num_stars; i++) {
		glColor3f(1.0f,1.0f,1.0f);
		glBegin(GL_POINTS);
		glVertex2f(pos[i], pos[i]*angle[i]);
		glVertex2f(pos[i]*angle[i], pos[i]);
		glEnd();
		pos[i] += spd[i];

		// if a star travels out of the screen, reset/reseed its variables
		if (pos[i] > 1.0f || pos[i]*angle[i] > 1 || pos[i] < -1.0f || pos[i]*angle[i] < -1.0f) {
			pos[i] = 0.0f;
			spd[i] = (((float)(((rand())%9)-4.0f))/1000.0f);
			angle[i] = pow((((float)((rand()%353)-171.0f))/100.0f), 3);
			if (spd[i]==0.0f) {spd[i] = 0.001;}
		}
	}

	const float rad = PI/180;				//constant to convert degrees to radians

	// draws an ellipse
	glColor3f(0.9f, 0.7f, 0.0f);			//sets color to dark yellow (gold)
	float major_radius = 0.5f;				//radii
	float minor_radius = 0.3f;
	glBegin(GL_POLYGON);
	for (int i=0; i<360; i++) {
		glVertex2f(cos(i*rad)*major_radius, sin(i*rad)*minor_radius);
	}
	glEnd();

	// draws another ellipse inside the previous one
	glColor3f(0.7f, 0.55f, 0.0f);				//sets color to bright yellow
	major_radius = 0.45f;
	minor_radius = 0.25f;
	glBegin(GL_POLYGON);
	for (int i=0; i<360; i++) {
		glVertex2f(cos(i*rad)*major_radius, sin(i*rad)*minor_radius);
	}
	glEnd();

	// draws a half ellipse inside the previous one
	glColor3f(1.0f, 0.8f, 0.1f);				//sets color to dark yellow
	major_radius = 0.42f;
	minor_radius = 0.25f;
	glBegin(GL_POLYGON);
	for (int i=90; i<271; i++) {
		glVertex2f(cos(i*rad)*major_radius, sin(i*rad)*minor_radius);
	}
	glEnd();

	// draws another half ellipse
	glColor3f(1.0f, 0.8f, 0.1f);				//sets color to dark yellow
	major_radius = 0.45f;
	minor_radius = 0.25f;
	glBegin(GL_POLYGON);
	for (int i=270; i<451; i++) {
		glVertex2f(cos(i*rad)*major_radius, sin(i*rad)*minor_radius);
	}
	glEnd();

	// arrays to hold color and vertex values
	float light_grey[3] = {0.9f,0.9f,0.9f};
	float darker_grey[3] = {0.6f,0.6f,0.6f};
	float dark_grey[3] = {0.3f,0.3f,0.3f};
	float vertices[7][3][3] = { // stores vertices for all the triangle polygons
		{{-0.4f, -0.5f, 0.0f}, {0.0f, 0.55f, 0.0f}, {0.05f, -0.2f, 0.0f}},
		{{0.4f, -0.45f, 0.0f}, {0.0f, 0.55f, 0.0f}, {0.05f, -0.2f, 0.0f}},
		{{-0.3f, -0.375f, 0.0f},{0.0f, 0.45f, 0.0f},{0.05f, -0.15f, 0.0f}},
		{{0.33f, -0.365f, 0.0f}, {0.0f, 0.45f, 0.0f}, {0.05f, -0.15f, 0.0f}},
		{{-0.05f, -0.1f, 0.0f}, {0.0f, 0.35f, 0.0f}, {0.00f, -0.01f, 0.0f}},
		{{0.05f, -0.1f, 0.0f}, {0.0f, 0.35f, 0.0f}, {0.00f, -0.01f, 0.0f}},
		{{0.1f, 0.05f, 0.0f}, {0.00f, -0.01f, 0.0f}, {-0.1f, 0.05f, 0.0f}},
	};

	// loop to draw above triangles
	for(int i = 0; i<7; i++) {

		float* c;
		if (i<2) {c = light_grey;}
		else if (i<4) {c = darker_grey;}
		else {c = dark_grey;}

		glColor3f(c[0], c[1], c[2]);
		glBegin(GL_POLYGON);
		glVertex3f(vertices[i][0][0], vertices[i][0][1], vertices[i][0][2]);
		glVertex3f(vertices[i][1][0], vertices[i][1][1], vertices[i][1][2]);
		glVertex3f(vertices[i][2][0], vertices[i][2][1], vertices[i][2][2]);
		glEnd();
	}

	glFlush();
	glutSwapBuffers();                           // swap buffers (we earlier set double buffer)
}

//---------------------------------------------------------------------------------


//****************************************************
// called by glut when there are no messages to handle
//****************************************************
void myFrameMove() {
	//nothing here for now
#ifdef _WIN32
	Sleep(10);                                   //give ~10ms back to OS (so as not to waste the CPU)
#endif
	glutPostRedisplay(); // forces glut to call the display function (myDisplay())
}


//****************************************************
// the usual stuff, nothing exciting here
//****************************************************
int main(int argc, char *argv[]) {
	//This initializes glut
	glutInit(&argc, argv);

	//This tells glut to use a double-buffered window with red, green, and blue channels 
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);

	// Initalize theviewport size
	viewport.w = 400;
	viewport.h = 400;

	//The size and position of the window
	glutInitWindowSize(viewport.w, viewport.h);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("CS184!");

	initScene();                                 // quick function to set up scene

	glutDisplayFunc(myDisplay);                  // function to run when its time to draw something
	glutReshapeFunc(myReshape);                  // function to run when the window gets resized
	glutIdleFunc(myFrameMove);                   // function to run when not handling any other task
	glutMainLoop();                              // infinite loop that will keep drawing and resizing and whatever else

	return 0;
}








